<?php
        
        //cek user aktif
        //session_start();

        if(isset($_SESSION['login']) and !empty($_SESSION['login'])){
        }
        else{
                redirect('CLogin/logout');  
        }
?>    
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Info Pasar | Dashboard</title>
        <!--<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <link rel="stylesheet" 
        href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
        <script type="text/javascript" 
        src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" 
        src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script>
        $(document).ready(function(){
            $('#tableKomoditas').dataTable();
        });
        </script>

        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Icon Tab aplikasi-->
        <link rel="shortcut icon" href="<?php echo base_url();?>assets/dist/img/inpas.png" />        
        <!-- Bootstrap 3.3.4 -->
        <link href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
        <!-- Bootstrap 3.3.4 -->
        <link href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />        
        <!-- FontAwesome 4.3.0 -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons 2.0.0 -->
        <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />    
        <!-- Theme style -->
        <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
            folder instead of downloading all of them to reduce the load. -->
        <link href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="<?php echo base_url(); ?>assets/plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="<?php echo base_url(); ?>assets/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- Date Picker -->
        <link href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
                $(document).ready(function() {
                  $("#id_provinsi").change(function() {
                      var provinsi =$("#id_provinsi").val();
                      $.ajax({
                              type: "POST",
                              url: "<?php echo site_url('CPasar/getKota');?>",
                              data: {id_provinsi:provinsi},
                              cache: false,
                              success: function(html) {
                                      $("#id_kota").html(html);
                              } 
                      });
                      $.ajax({
                              type: "POST",
                              url: "<?php echo site_url('CPasar/getDailyCombo');?>",
                              data: {id_provinsi:provinsi},
                              cache: false,
                              success: function(html) {
                                      $("#tableip").html(html);
                              } 
                      });
                    });
                  $("#id_kota").change(function() {
                                var kot =$("#id_kota").val();
                                var provinsi = $("#id_provinsi").val();
                                $.ajax({
                                        type: "POST",
                                        url: "<?php echo site_url('CPasar/getPasarbyKotaandProvinsi');?>",
                                        data: {id_kota:kot,id_provinsi:provinsi},
                                        cache: false,
                                        success: function(html) {
                                                $("#id_pasar").html(html);
                                        } 
                                });
                                $.ajax({
                                      type: "POST",
                                      url: "<?php echo site_url('CPasar/getDailyCombo');?>",
                                      data: {id_kota:kot,id_provinsi:provinsi},
                                      cache: false,
                                      success: function(html) {
                                              $("#tableip").html(html);
                                      } 
                                });
                    });
                    $("#id_pasar").change(function() {
                                var kot =$("#id_kota").val();
                                var provinsi = $("#id_provinsi").val();
                                var pasar = $("#id_pasar").val();
                                $.ajax({
                                      type: "POST",
                                      url: "<?php echo site_url('CPasar/getDailyCombo');?>",
                                      data: {id_pasar:pasar,id_kota:kot,id_provinsi:provinsi},
                                      cache: false,
                                      success: function(html) {
                                              $("#tableip").html(html);
                                      } 
                                });
                    });
                });
        </script>
    </head>
    <body class="skin-blue sidebar-mini">
        <div class="wrapper">

            <?php $this->load->view('header'); ?>
            <?php $this->load->view('sidebar'); ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                    <section class="content-header">
                    <h1>
                        <?php echo $active_menu;?>
                    </h1>
                    </section>
                
                    <!-- Main content -->
                    <section class="content bg-content">
                        <!-- Small boxes (Stat box) -->
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                              <a href="<?php echo site_url('CKomoditas');?>" class="btn btn-primary btn-block margin-bottom komunitas">Komoditas</a>
                            </div><!-- /.col -->
                            <div class="col-md-3 col-sm-6 col-xs-12">
                              <a href="<?php echo site_url('CPasar');?>" class="btn btn-primary btn-block margin-bottom pasar">Pasar</a>
                            </div><!-- /.col -->
                            <div class="col-md-3 col-sm-6 col-xs-12">
                              <a href="<?php echo site_url('CPasar/infoPasar');?>" class="btn btn-primary btn-block margin-bottom ip">Info Pasar Terkini</a>
                            </div><!-- /.col -->
                            <div class="col-md-3 col-sm-6 col-xs-12">
                              <a href="<?php echo site_url('CPasar/history');?>" class="btn btn-primary btn-block margin-bottom history">History Komoditas</a>
                            </div><!-- /.col -->
                            
                        </div><!-- /.row -->
                    <!-- Main row -->
                    <div class="row">
                          <div class='row'>
                        <div class="col-lg-12 col-lg-8">
                            <!-- general form elements -->
                              <div class="box box-primary">
                                <div class="box-header">
                                  <h3 class="box-title">Surveyor Baru</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" method='post' action='<?php echo site_url('CSurveyor/add');?>'>
                                  <div class="box-body">
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Email address</label>
                                      <input name='email_surveyor' type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" required>
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputPassword1">Username</label>
                                      <input name='username_surveyor' type="text" class="form-control" id="exampleInputPassword1" placeholder="Username" required>
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputPassword1">Nama</label>
                                      <input name='nama_surveyor' type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama" required>
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputPassword1">Password</label>
                                      <input name='password_surveyor' type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputPassword1">Re-type Password</label>
                                      <input name='repassword_surveyor' type="password" class="form-control" id="exampleInputPassword1" placeholder="Ketik Ulang Password" required>
                                    </div>
                                    <!-- select -->
                                    <div class="form-group">
                                      <label>Provinsi</label>
                                      <select name='provinsi_surveyor' class="form-control" required id="id_provinsi">
                                        <option value=''>Pilih Provinsi</option>
                                        <?php foreach ($provinsi as $key) {?>
                                            <option value="<?php echo $key['id_provinsi'];?>"><?php echo $key['nama'];?></option>
                                        <?php }?>
                                      </select>
                                    </div>
                                    <!-- select -->
                                    <div class="form-group">
                                      <label>Kota</label>
                                    <select id="id_kota" class="form-control" name="kota_surveyor">
                                    <option value="">Pilih Kota</option>
                                    </select>
                                    </div>
                                    <!-- select -->
                                    <div class="form-group">
                                      <label>Pasar</label>
                                    <select id="id_pasar" class="form-control" name="pasar_surveyor">
                                    <option value="">Pilih Pasar</option>
                                    </select>
                                    </div>
                                  </div><!-- /.box-body -->

                                  <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                  </div>
                                </form>
                              </div><!-- /.box -->
                        </div><!-- /.col -->
                    </div>
                    </div><!-- /.row (main row) -->


                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->

            <?php $this->load->view('footer');?>
            <!-- Add the sidebar's background. This div must be placed
                immediately after the control sidebar -->
            <div class='control-sidebar-bg'></div>
        </div><!-- ./wrapper -->

        <!-- jQuery 2.1.4 -->
        <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- jQuery UI 1.11.2 -->
        <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.2 JS -->
        <!--<script src="<?php //echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    -->
        <!-- Morris.js charts -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/morris/morris.min.js" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="<?php echo base_url(); ?>assets/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="<?php echo base_url(); ?>assets/plugins/knob/jquery.knob.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- datepicker -->
        <script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- Slimscroll -->
        <script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <!-- FastClick -->
        <script src='<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.min.js'></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js" type="text/javascript"></script>    

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="<?php echo base_url(); ?>assets/dist/js/pages/dashboard.js" type="text/javascript"></script>    

        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>assets/dist/js/demo.js" type="text/javascript"></script>
    </body>
</html>