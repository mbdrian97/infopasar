<?php ?>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="<?php echo base_url();?>jquery.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <link rel="stylesheet" 
        href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
        <script type="text/javascript" 
        src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" 
        src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

         <!-- jQuery 2.1.4 -->
        <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- jQuery UI 1.11.2 -->
        <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script>
        <script>
        $(document).ready(function(){
            $('#tablePasar').dataTable();
        });
        </script>
        <!-- Bootstrap 3.3.4 -->
        
</head>
<body>
						<div class="table-responsive">
                            <table id="tablePasar" class="table table-bordered table-striped">
                              <thead>
                                <tr>
                                  <th width=40px><center>#</center></th>
                                  <th width=200px>Komoditas</th>
                                  <th>Harga Tertinggi</th>
                                  <th>Harga Terendah</th>
                                  <th>Harga Rata-rata</th>
                                  <th>Harga Terakhir</th>
                                </tr>
                              </thead>
                              <tbody>
                              <?php $i=1;foreach ($hasil as $row) {?>
                                <tr><h3>
                                  <td><center><?php echo $i;?></center></td>
                                  <td><?php echo $row['namaKomoditas'];?></td>
                                  <td><a data-toggle="modal" class='edit' href="#showhighestprice-<?php echo $i;?>"><?php echo format_rupiah($row['harga_max']);?></a>
                                  		<!--Modal untuk harga Tertinggi-->
				                            <div class="modal fade" id="showhighestprice-<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				                              <div class="modal-dialog">
				                                <div class="modal-content">
				                                  <div class="modal-header">
				                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				                                    <h4 class="modal-title" id="myModalLabel">Lokasi Pasar untuk Harga Tertinggi</h4>
				                                  </div>
				                                  <div class="modal-body">
				                                  <!--============================-->
				                                    <div class="box">
				                                        <div class="box-header with-border">
				                                          <h1 class="box-title">Harga <?php echo "Rp. ".format_rupiah($row['harga_max']).",00";?></h1>
				                                        </div><!-- /.box-header -->
				                                        <div class="box-body no-padding">
				                                        	<table class="table table-striped">
											                    <tr>
											                      <th style="width: 10px">#</th>
											                      <th>Nama Pasar</th>
											                      <th>Kota</th>
											                      <th>Provinsi</th>
											                    </tr>
											                    <?php $a=1;foreach ($row['pasar4'] as $key) {?>
											                    <tr>
											                      <td><?php echo $a.'.';?></td>
											                      <td><?php echo $key['pasar'];?></td>
											                      <td><?php echo $key['kota'];?></td>
											                      <td><?php echo $key['provinsi'];?></td>
											                    </tr>
											                    <?php $a++;}?>
											                </table>
				                                        </div><!-- /.box-body -->
				                                      </div><!-- /.box -->
				                                    <!--============================-->
				                                  </div>
				                                  <div class="modal-footer">
				                                    <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Close</button>
				                                  </div>
				                                  </form>
				                                </div><!-- /.modal-content -->
				                              </div><!-- /.modal-dialog -->
				                            </div><!-- /.modal -->

                                  </td>
                                  <td><a data-toggle="modal" class='edit' href="#showlowestprice-<?php echo $i;?>"><?php echo format_rupiah($row['harga_min']);?></a>
                                  		<!--Modal untuk harga terendah-->
				                            <div class="modal fade" id="showlowestprice-<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				                              <div class="modal-dialog">
				                                <div class="modal-content">
				                                  <div class="modal-header">
				                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				                                    <h4 class="modal-title" id="myModalLabel">Lokasi Pasar untuk Harga Terendah</h4>
				                                  </div>
				                                  <div class="modal-body">
				                                  <!--============================-->
				                                    <div class="box">
				                                        <div class="box-header with-border">
				                                          <h1 class="box-title">Harga <?php echo "Rp. ".format_rupiah($row['harga_min']).",00";?></h1>
				                                        </div><!-- /.box-header -->
				                                        <div class="box-body no-padding">
				                                        	<table class="table table-striped">
											                    <tr>
											                      <th style="width: 10px">#</th>
											                      <th>Nama Pasar</th>
											                      <th>Kota</th>
											                      <th>Provinsi</th>
											                    </tr>
											                    <?php $a=1;foreach ($row['pasar5'] as $key) {?>
											                    <tr>
											                      <td><?php echo $a.'.';?></td>
											                      <td><?php echo $key['pasar'];?></td>
											                      <td><?php echo $key['kota'];?></td>
											                      <td><?php echo $key['provinsi'];?></td>
											                    </tr>
											                    <?php $a++;}?>
											                </table>
				                                        </div><!-- /.box-body -->
				                                      </div><!-- /.box -->
				                                    <!--============================-->
				                                  </div>
				                                  <div class="modal-footer">
				                                    <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Close</button>
				                                  </div>
				                                  </form>
				                                </div><!-- /.modal-content -->
				                              </div><!-- /.modal-dialog -->
				                            </div><!-- /.modal -->
                                  </td>
                                  <td><?php echo format_rupiah($row['harga_avg']);?></td>
                                  <td><a data-toggle="modal" class='edit' href="#showlastprice-<?php echo $i;?>"><?php echo format_rupiah($row['last_harga']);?></a>
                                  		<!--Modal untuk harga terendah-->
				                            <div class="modal fade" id="showlastprice-<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				                              <div class="modal-dialog">
				                                <div class="modal-content">
				                                  <div class="modal-header">
				                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				                                    <h4 class="modal-title" id="myModalLabel">Lokasi Pasar untuk Harga Terakhir</h4>
				                                  </div>
				                                  <div class="modal-body">
				                                  <!--============================-->
				                                    <div class="box">
				                                        <div class="box-header with-border">
				                                          <h1 class="box-title">Harga <?php echo "Rp. ".format_rupiah($row['last_harga']).",00";?></h1>
				                                        </div><!-- /.box-header -->
				                                        <div class="box-body no-padding">
				                                        	<table class="table table-striped">
											                    <tr>
											                      <th>Kode Pasar</th>
											                      <th>Nama Pasar</th>
											                      <th>Kota</th>
											                      <th>Provinsi</th>
											                    </tr>
											                    <tr>
											                    	<?php foreach ($row['pasar3'] as $key) {?>
											                    		<td><?php echo $key;?></td>
											                    	<?php }?>
											                    </tr>
											                </table>
				                                        </div><!-- /.box-body -->
				                                      </div><!-- /.box -->
				                                    <!--============================-->
				                                  </div>
				                                  <div class="modal-footer">
				                                    <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Close</button>
				                                  </div>
				                                  </form>
				                                </div><!-- /.modal-content -->
				                              </div><!-- /.modal-dialog -->
				                            </div><!-- /.modal -->
                                  </td></h3>
                                </tr>
                                <?php $i++;}?>
                              </tbody>
                            </table>
                          </div>  
</body>
</html>

<?php
	function format_rupiah($angka){
	  $rupiah=number_format($angka,0,',','.');
	  return $rupiah;
	  }
?>
