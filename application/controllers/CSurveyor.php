<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CSurveyor extends CI_Controller{

    public function __construct(){
        parent::__construct();
         $this->load->model('MPasar');
         $this->load->model('MSurveyor');
    }

    public function index(){
    	$this->load->library('session');
    	$data['surveyor'] = $this->MSurveyor->getUser();
        $data['provinsi'] = $this->MPasar->getData('tb_provinsi');
        $data['kota'] = $this->MPasar->getData('tb_kota');
        $data['pasar1'] = $this->MPasar->getData('tb_dataPasar');
        $data['active_menu'] = "Surveyor";
        $this->load->view('VSurveyor',$data);
    }

    public function add_surveyor(){
    	$this->load->library('session');
    	$data['provinsi'] = $this->MPasar->getData('tb_provinsi');
        $data['active_menu'] = "Tambah Surveyor";
        $this->load->view('VAddSurveyor',$data);	
    }

    public function add(){
    	if ($this->input->post('password_surveyor') == $this->input->post('repassword_surveyor')) {
    		$surveyor = array(
	    		"email" => $this->input->post('email_surveyor'),
	    		"username" => $this->input->post('username_surveyor'),
	    		"password" => sha1($this->input->post('password_surveyor')),
	    		"nama" => $this->input->post('nama_surveyor'),
	    		"provinsi" => $this->input->post('provinsi_surveyor'),
	    		"kota" => $this->input->post('kota_surveyor'),
	    		"pasar" => $this->input->post('pasar_surveyor')
	    	);	
	    	if ($this->MSurveyor->insert($surveyor)) {
	    		echo "<script type='text/javascript'>alert('Surveyor berhasil ditambahkan')
                            window.location = '".site_url('CSurveyor')."';
                            </script>";
	    	}
	    	
    	} else {
    		echo "<script type='text/javascript'>alert('Password tidak sama. Harap Ulangi')
                            window.location = '".site_url('CSurveyor')."';
                            </script>";
    	}
    }

    public function operasi(){
        if ($this->input->post('ubahSandi')=="Ubah Sandi") {
            echo "<script type='text/javascript'>alert('E1dit surveyor')
                            window.location = '".site_url('CSurveyor')."';
                            </script>";
        } elseif ($this->input->post('edit_surveyor')=="Edit!") {
            $id_user = $this->input->post('id_surveyor');
            $surveyor = array(
                "email" => $this->input->post('email_surveyor'),
                "username" => $this->input->post('username_surveyor'),
                "nama" => $this->input->post('nama_surveyor'),
                "provinsi" => $this->input->post('provinsi_surveyor'),
                "kota" => $this->input->post('kota_surveyor'),
                "pasar" => $this->input->post('pasar_surveyor')
            );  
            if ($this->MSurveyor->update($surveyor,$id_user)) {
                echo "<script type='text/javascript'>alert('berhasil')
                            window.location = '".site_url('CSurveyor')."';
                            </script>";
            }
        } elseif ($this->input->post('delete_surveyor')=="Delete") {
            $id_user = $this->input->post('id_surveyor');
            $this->MSurveyor->delete($id_user);
            echo "<script type='text/javascript'>alert('delete surveyor')
                            window.location = '".site_url('CSurveyor')."';
                            </script>";
        }
    }
}

/* End of file CSurveyor.php */
/* Location: ./application/controllers/CSurveyor.php */