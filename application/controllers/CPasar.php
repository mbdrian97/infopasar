<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CPasar extends CI_Controller{
    private $data;
    private $refresh;

    public function __construct(){
        parent::__construct();
        $this->load->model('MPasar');
        $this->load->model('MKomoditas');
        $this->load->helper('url_helper');
        $this->load->library('session');
    }

    public function index(){
        $data['active'] = "Pasar";
        $data['pasar'] = $this->MPasar->getPasar();
        $data['active_menu'] = 'Pasar';
        $data['provinsi'] = $this->MPasar->getData('tb_provinsi');
        $this->load->view('VDataPasar',$data);
    }
    
    public function getPasarbyKotaandProvinsi(){
        if (isset($_POST['id_kota'])) {
            $id_kota = $this->input->post('id_kota');
            $id_provinsi = $this->input->post('id_provinsi');
            $dataPasar = $this->MPasar->getPasarbyKotaandProvinsi($id_kota,$id_provinsi);
            echo "<option value=''>Pilih Pasar</option>";
            foreach($dataPasar as $row){
                echo '<option value='.$row['id_pasar'].'>'.$row['pasar'].'</option>';        
            }
        }
    }

    public function getSortingPasar(){
        echo "<thead>
                <tr>
                  <th width=20px>#</th>
                  <th>Nama Pasar</th>
                  <th>Kota</th>
                  <th>Provinsi</th>
                </tr>
              </thead>
              <tbody>";
              $i=1; 
        if(isset($_POST['kotPasar']) and isset($_POST['provPasar'])) {
            $id_kota = $this->input->post('kotPasar');
            $id_provinsi = $this->input->post('provPasar');
            $dataPasar = $this->MPasar->getPasarbyKotaandProvinsi($id_kota,$id_provinsi);
        }elseif (isset($_POST['provPasar'])){
            $id_provinsi = $this->input->post('provPasar');
            $dataPasar = $this->MPasar->getPasarbyProvinsi($id_provinsi);
        }
        
        foreach($dataPasar as $row) {
            echo"<tr>
                <td>".$i."</td>
                <td>".$row['pasar']."</td>
                <td>".$row['kota']."</td>
                <td>".$row['provinsi']."</td>";
                $i++; 
            }
         echo "</tbody>";
    }
    
    public function infoPasar(){
        $data['active'] = "Daily Info Pasar";
        $data['active_menu'] = "Daily Info Pasar";
        $data['provinsi'] = $this->MPasar->getData('tb_provinsi');
        $data['komoditas'] = $this->MKomoditas->getKomoditas();
        $tempPasar = array();
        $day = date('d');
        $array = array();
        $tampRef = $this->MPasar->getKetRefresh();
        if($tampRef){
            $this->refresh = $tampRef['refresh'];
        }else {
            $this->MPasar->insertKetRefresh(); 
            $this->refresh = 'belum';
        }
        
        $data['id_hari'] = date('w');
        $data['id_tanggal']['today'] = date('Y-m-d');
        $jamSekarang = date('H:i:s', time());
        if($jamSekarang>='16:00:00' and $this->refresh=='belum'){
            $this->getDailyInfo();
            $this->MPasar->updateKetRefresh();
        } else if($jamSekarang<="16:00:00") {
            $day--;
            $data['id_hari']--;
            $data['id_tanggal'] = $this->MPasar->getTime($data['id_tanggal']['today']);
        }
        $data['id_hari'] %=7;
        $data['daily'] = $this->MPasar->getInfoDaily($day);
        $indeks = 0;
        foreach ($data['daily'] as $daily) {
            # code...
                //print_r($daily);
                for ($i=4; $i <= 5; $i++) { 
                        $array = unserialize($daily['pasar'.$i.'']);
                        foreach ($array as $key) {
                            $tempPasar[] = $this->MPasar->getPasarbyId($key);
                        }
                        $data['daily'][$indeks]['pasar'.$i.''] = $tempPasar;
                        $tempPasar = array();
                    }
                    $data['daily'][$indeks]['pasar3'] = $this->MPasar->getPasarbyId($daily['pasar3']);
            $indeks++;
        }
        $this->load->view('VInfoPasar',$data);
    }
    
    public function getDailyInfo(){
        $listKomoditas = $this->MKomoditas->getKomoditas();
        //print_r($listKomoditas);
        foreach($listKomoditas as $list){
            $hasil = $this->getDaily(1,$list['id_Komoditas'],0,0,0);
            if ($hasil['last_harga']!=0){
                $this->MPasar->insertDailyInfoPasar($hasil);
            }
        }
    }

    public function getDailyCombo(){
        $data = array();
        $tempPasar = array();
        $listKomoditas = $this->MKomoditas->getKomoditas();
        if(isset($_POST['id_provinsi']) and isset($_POST['id_kota']) and isset($_POST['id_pasar'])) {
            $id_prov = $this->input->post('id_provinsi');
            $id_kot = $this->input->post('id_kota');
            $id_pas = $this->input->post('id_pasar');
            foreach ($listKomoditas as $list) {
                $hasil = $this->getDaily(4,$list['id_Komoditas'],$id_prov,$id_kot,$id_pas);
                if ($hasil['last_harga']!=0) {
                    $hasil['namaKomoditas'] = $list['nama_Komoditas'];
                    for ($i=4; $i <= 5; $i++) { 
                        foreach ($hasil['pasar'.$i.''] as $key) {
                            $tempPasar[] = $this->MPasar->getPasarbyId($key);
                        }
                        $hasil['pasar'.$i.''] = $tempPasar;
                        $tempPasar = array();
                    }
                    $hasil['pasar3'] = $this->MPasar->getPasarbyId($hasil['pasar3']);
                    $data[] = $hasil;
                }
            }
        }elseif(isset($_POST['id_provinsi']) and isset($_POST['id_kota'])) {
            $id_prov = $this->input->post('id_provinsi');
            $id_kot = $this->input->post('id_kota');
            foreach ($listKomoditas as $list) {
                $hasil = $this->getDaily(3,$list['id_Komoditas'],$id_prov,$id_kot,0);
                if ($hasil['last_harga']!=0) {
                    $hasil['namaKomoditas'] = $list['nama_Komoditas'];
                    for ($i=4; $i <= 5; $i++) { 
                        foreach ($hasil['pasar'.$i.''] as $key) {
                            $tempPasar[] = $this->MPasar->getPasarbyId($key);
                        }
                        $hasil['pasar'.$i.''] = $tempPasar;
                        $tempPasar = array();
                    }
                    $hasil['pasar3'] = $this->MPasar->getPasarbyId($hasil['pasar3']);
                    $data[] = $hasil;
                }
            }
        }elseif (isset($_POST['id_provinsi'])) {
            $id_prov = $this->input->post('id_provinsi');
            //print_r($listKomoditas);
            foreach($listKomoditas as $list){
                $hasil = $this->getDaily(2,$list['id_Komoditas'],$id_prov,0,0);
                if ($hasil['last_harga']!=0) {
                    $hasil['namaKomoditas'] = $list['nama_Komoditas'];
                    for ($i=4; $i <= 5; $i++) { 
                        foreach ($hasil['pasar'.$i.''] as $key) {
                            $tempPasar[] = $this->MPasar->getPasarbyId($key);
                        }
                        $hasil['pasar'.$i.''] = $tempPasar;
                        $tempPasar = array();
                    }
                    $hasil['pasar3'] = $this->MPasar->getPasarbyId($hasil['pasar3']);
                    $data[] = $hasil;
                }
            }
        }
        
        $result['hasil'] = $data;
        $this->load->view('tableinfopasar',$result);
    }

    private function getDaily($kode,$ko,$pr,$kot,$pa){
        if ($kode == 1) {
            $datas = $this->MPasar->getInfoPasarByKomoditas($ko);
        }elseif ($kode == 2) {
            $datas = $this->MPasar->getIPbyKomandPro($ko,$pr);
        }elseif ($kode == 3) {
            $datas = $this->MPasar->getIPbyKomProKot($ko,$pr,$kot);
        }elseif ($kode == 4) {
            $datas = $this->MPasar->getIPbyKomProKotPas($ko,$pr,$kot,$pa);
        }
        $tempMax = -9999999;
        $tempMin = 9999999999999;
        $tempPasarMax = array();
        $tempPasarMin = array();
        
       //id Komoditas
        $hasil['tanggal'] = date("Y-m-d");
        $hasil['id_Komoditas'] = $ko;
        $tempKomoditas = $this->MKomoditas->getKomoditasbyIndex($ko);
        //get harga max
        if($datas){
            foreach($datas as $data){
                if($data['harga_Komoditas']>$tempMax) {
                    $tempMax = $data['harga_Komoditas'];
                    $tempPasarMax = array();
                    $tempPasarMax[] = $data['id_pasar'];
                }elseif ($data['harga_Komoditas']==$tempMax) {
                    $tempPasarMax[] = $data['id_pasar'];
                }
            }
            $hasil['harga_max'] = $tempMax;

            foreach($datas as $data){
                if($data['harga_Komoditas']<$tempMin) {
                    $tempMin = $data['harga_Komoditas']; 
                    $tempPasarMin = array();
                    $tempPasarMin[] = $data['id_pasar'];
                }elseif ($data['harga_Komoditas']==$tempMin) {
                    $tempPasarMin[] = $data['id_pasar'];
                }
            }
            $hasil['harga_min'] = $tempMin;
            $hasil['last_harga'] = $datas[0]['harga_Komoditas'];
            $hasil['pasar3'] = $datas[0]['id_pasar'];

            if ($kode == 1) {
                /*$escaped = array_map('mysql_escape_string',array_values($tempPasarMax));
                $values = implode(', ', $escaped);
                $hasil['pasar1'] = $values;
                $escaped = array_map('mysql_escape_string',array_values($tempPasarMin));
                $values = implode(', ', $escaped);
                $hasil['pasar2'] = $values;*/
                $hasil['pasar4'] = serialize($tempPasarMax);
                $hasil['pasar5'] = serialize($tempPasarMin);
            }else{
                $hasil['pasar4'] = $tempPasarMax;
                $hasil['pasar5'] = $tempPasarMin;
            }
        }else {
            $hasil['harga_max'] = 0;
            $hasil['harga_min'] = 0;
            $hasil['last_harga'] = 0;
            $hasil['pasar4'] = array('-');
            $hasil['pasar5'] = array('-');
        }

        //get rata-rata harga Komoditas
        $hasil['harga_avg'] = ($hasil['harga_min']+ $hasil['harga_max'])*0.5;
        return $hasil;
    }
    
    public function history(){
        $data['active'] = "History Info Pasar";
        $data['rows'] = $this->MPasar->getInfoPasar();
        $data['komoditas'] = $this->MKomoditas->getKomoditas();
        $data['provinsi'] = $this->MPasar->getData('tb_provinsi');
        $data['active_menu'] = "History Info Pasar";
        
        $this->load->view('VHistory',$data);    
    }
    
    public function add(){
      if(isset($_POST['kotaPasar']) and isset($_POST['namaPasar']) and isset($_POST['provinsiPasar'])){
            $data['id_admin']= $this->session->userdata('id');
            $data['nama']= $this->input->post('namaPasar');
            $data['provinsi'] = $this->input->post('provinsiPasar');
            $data['kota'] = $this->input->post('kotaPasar');
            $this->MPasar->insertPasar($data);
            echo "<script type='text/javascript'>alert('Pasar berhasil ditambahkan')
                            window.location = '".site_url('CPasar')."';
                            </script>";
        } else {
            echo "<script type='text/javascript'>alert('Data tidak boleh kosong')
                            window.location = '".site_url('CPasar')."';
                            </script>";
        }
    }
    
    public function edit(){
        if(isset($_POST['editPasar'])){
            $data['nama'] = $this->input->post('namaPasar');
            $data['provinsi'] = $this->input->post('provinsiPasar');
            $data['kota'] = $this->input->post('kotaPasar');
            $id = $this->input->post('id');
            $this->MPasar->updatePasar($data,$id);
            echo "<script type='text/javascript'>alert('Data Pasar berhasil Diubah')
                        window.location = '".site_url('CPasar')."';
                        </script>";
        }else if(isset($_POST['deletePasar'])){
            $id = $this->input->post('id');
            $this->MPasar->deletePasar($id);
            echo "<script type='text/javascript'>alert('Data Pasar berhasil dihapus')
                        window.location = '".site_url('CPasar')."';
                        </script>";
        }  
    }
    
    public function getKota(){
        if($_POST['id_provinsi']){
            $id_prov = $this->input->post('id_provinsi');
            $kota = $this->MPasar->getKota($id_prov);
            echo "<option value=''>Pilih Kota</option>";
            foreach($kota as $row){
                echo '<option value='.$row['id_kota'].'>'.$row['nama'].'</option>';        
            }
        }else if ($_POST['provPasar']){
            $id_prov = $this->input->post('provPasar');
            $kota = $this->MPasar->getKota($id_prov);
            echo "<option value=''>Pilih Kota</option>";
            foreach($kota as $row){
                echo '<option value='.$row['id_kota'].'>'.$row['nama'].'</option>';        
            }
        }if($_POST['idx_provinsi']){
            $id_prov = $this->input->post('idx_provinsi');
            $kota = $this->MPasar->getKota($id_prov);
            echo "<option value=''>Pilih Kota</option>";
            foreach($kota as $row){
                echo '<option value='.$row['id_kota'].'>'.$row['nama'].'</option>';        
            }
        }
    }

    public function getInfoPasar(){
        echo "<thead>
                    <tr>
                      <th width=20px>#</th>
                      <th>Komoditas</th>
                      <th>Waktu</th>
                      <th width=100px>Harga (Rp/Kg)</th>
                      <th>Pasar</th>
                    </tr>
                  </thead>
                  <tbody>";
            $i=1; 
        if (isset($_POST['id_komoditas']) and isset($_POST['id_pasar'])) {
            $id_kom = $this->input->post('id_komoditas');
            $id_pas = $this->input->post('id_pasar');
            $info = $this->MPasar->getInfoByKomoditasandPasar($id_kom,$id_pas);
        }elseif (isset($_POST['id_pasar'])) {
            $id_pas = $this->input->post('id_pasar');
            $info = $this->MPasar->getInfoByPasar($id_pas);
        }elseif (isset($_POST['id_provinsi']) and isset($_POST['id_kota'])) {
            $id_prov = $this->input->post('id_provinsi');
            $id_kota = $this->input->post('id_kota');
            $info = $this->MPasar->getInfobyProvinsiandKota($id_kota,$id_prov);
        }elseif (isset($_POST['id_provinsi'])) {
            $id_prov = $this->input->post('id_provinsi');
            $info = $this->MPasar->getInfoByProvinsi($id_prov);
        }
        foreach($info as $row) {
            echo"<tr>
                <td>".$i."</td>
                <td>".$row['Komoditas']."</td>
                <td>".$row['waktu']."</td>
                <td align='right'>".$this->format_rupiah($row['harga_Komoditas'])."</td>
                <td>".$row['pasar'].", ".$row['kota'].", ".$row['provinsi']."</td>
                </tr>";
                $i++; 
        }
        echo "</tbody>";
    }

    private function format_rupiah($angka){
        $rupiah=number_format($angka,0,',','.');
        return $rupiah;
    }
    
    public function getChart(){
        if($_POST['kom']==""){
            echo "<script type='text/javascript'>alert('Komoditas tidak boleh kosong')
                            window.location = '".site_url('CPasar/history')."';
                            </script>";
        }
        else if ($_POST['namaBulan']==""){
            echo "<script type='text/javascript'>alert('Periode bulan tidak boleh kosong')
                            window.location = '".site_url('CPasar/history')."';
                            </script>";
        }
        else if($_POST['idx_week']==""){
            echo "<script type='text/javascript'>alert('Indeks Minggu tidak boleh kosong')
                            window.location = '".site_url('CPasar/history')."';
                            </script>";
        }
            $id_kom = $this->input->post('kom');
            $id_bulan = $this->input->post('namaBulan');
            $id_week = $this->input->post('idx_week');
            $this_year = date('Y');
            
            $data['active'] = "Grafik Harga Pasar";
            $namaKomoditas = $this->MKomoditas->getKomoditasbyIndex($id_kom);
            $data['Komoditas'] = $namaKomoditas['nama_Komoditas'];
            for($i=1;$i<=7;$i++){
                $result = $this->MPasar->getDatatoChart($id_kom,$id_bulan,$id_week,$this_year,$i);
                if($result){
                    $n = 0;
                    $tamp = 0;
                    foreach($result as $res){
                        $tamp += $res['harga_avg'];
                        $n++;
                    }
                    $data['hari'.$i] = $tamp/$n;
                }else $data['hari'.$i] = 0;
            }        
            
            
            $data['active_menu'] = "Grafik Harga <strong>".$namaKomoditas['nama_Komoditas']."</strong> Minggu <strong>ke- ".$id_week."</strong> Bulan <strong>".$this->getBulan($id_bulan)." - ".$this_year."</strong>";
            
            $this->load->view('grafikPasar',$data); 
    }
    
    private function getBulan($bulan){
                  if($bulan==1)return "Januari";
                  else if($bulan==2) return "Februari";
                  else if($bulan==3) return "Maret";
                  else if($bulan==4) return "April";
                  else if($bulan==5) return "Mei";
                  else if($bulan==6) return "Juni";
                  else if($bulan==7) return "Juli";
                  else if($bulan==8) return "Agustus";
                  else if($bulan==9) return "September";
                  else if($bulan==10) return "Oktober";
                  else if($bulan==11) return "November";
                  else if($bulan==12) return "Desember";
    }

}

/* End of file CPasar.php */
/* Location: ./application/controllers/CPasar.php */