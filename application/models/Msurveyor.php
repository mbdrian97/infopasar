<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MSurveyor extends CI_Model{
    private $info_pasar;
    private $nameTable = 'tb_user';

    public function __construct(){
        parent::__construct();
        $this->info_pasar = $this->load->database('info_pasar',TRUE);
    }

    public function getUser(){
        $table = $this->nameTable;

        $this->info_pasar->select('id_user');
        $this->info_pasar->select('email');
        $this->info_pasar->select('username');
        $this->info_pasar->select('password');
        $this->info_pasar->select('tb_user.nama as nama');
        $this->info_pasar->select('tb_dataPasar.nama as nama_pasar');
        $this->info_pasar->select('tb_dataPasar.id_pasar as pasar');
        $this->info_pasar->select('tb_kota.id_kota as kota');
        $this->info_pasar->select('tb_provinsi.id_provinsi as provinsi');
        $this->info_pasar->from($table);
        $this->info_pasar->join('tb_kota','tb_kota.id_kota=tb_user.kota');
        $this->info_pasar->join('tb_provinsi','tb_provinsi.id_provinsi=tb_user.provinsi');
        $this->info_pasar->join('tb_dataPasar','tb_dataPasar.id_pasar=tb_user.pasar');
        $query = $this->info_pasar->get();
        return $query->result_array();
    }

    public function insert($data){
        $table = $this->nameTable;

        $result = $this->info_pasar->insert($table,$data);
        return $result;
    }

    public function update($data,$id){
        $table = $this->nameTable;

        $this->info_pasar->where('id_user',$id);
        $this->info_pasar->update($table, $data);
        $result = TRUE;
        return $result;
    }

    public function delete($id){
        $table = $this->nameTable;

        $this->info_pasar->query("SET FOREIGN_KEY_CHECKS = 0");
        $this->info_pasar->where('id_user', $id);
        $this->info_pasar->delete($table); 
        $this->info_pasar->query("SET FOREIGN_KEY_CHECKS = 1");
    }

}

/* End of file MSurveyor.php */
/* Location: ./application/models/MSurveyor.php */